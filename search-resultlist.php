<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package aether
 */
 
	wp_enqueue_style( 'search-noresults', get_template_directory_uri() . '/css/search-resultlist.css' );

	global $query_string;

	$search_query = wp_parse_str( $query_string );
	$search       = new WP_Query( $search_query );

	get_search_form(); 
	get_template_part( 'parts/content-start' );

?>

<main class="<?php ae_main_classes(); ?>">

<section class="search-results">
<?php 

	while ( have_posts() ) { 
		the_post(); 
		get_template_part( 'parts/list-excerpt', get_post_format() ); 
	}

?>
</section>

<?php
	the_posts_pagination( array(
		'prev_text'          => __( 'Previous page', 'aether' ),
		'next_text'          => __( 'Next page', 'aether' ),
		'show_all' => true,
	) );
?>

</main>

<?php

	get_template_part( 'parts/content-end' );
