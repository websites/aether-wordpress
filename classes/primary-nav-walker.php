<?php
/**
 * @package aether
 */
 
namespace Aether;
 
class PrimaryNavwalker extends \Walker_Nav_Menu 
{
	private $m_item = null;
	private $m_topLevelItem = null;
	private $m_topLevelItemDepth = 0;
	private $m_menuSlug = '';
	private $m_menuData = null;
	
	public function __construct($slug) {
		$this->m_menuSlug = $slug;
		$this->m_menuData = $this->preprocessStructure(getMenuStructure($slug));
	}
	
	private function preprocessStructure($data) 
	{
		$output = [];
		
		foreach ($data as $m) {
			if ($m->menu_item_parent == 0) {
				$output[$m->ID] = $this->generateRecurse($m, $data);
				$output[$m->ID]['thumbnail'] = get_the_post_thumbnail_url($m->object_id, 'aether-primary-menu');
			}
		}
		
		return $output;
	}
	
	private function generateRecurse($menuObject, $data) {
		
		$output = [
			'title'       => $menuObject->title,
			'url'         => $menuObject->url,
			'type'        => $menuObject->type,
			'type_label'  => $menuObject->type_label,
			'thumbnail'   => null,
			'post_status' => $menuObject->post_status,
			'children'    => []
		];
		
		foreach ($data as $m) {
			if ($m->menu_item_parent != $menuObject->ID) {
				continue;
			}
			
			//echo $m->menu_item_parent .' -> '. $menuObject->ID . "\n";
			
			$output['children'][$m->ID] = $this->generateRecurse($m, $data);
			$output[$m->ID] = $output['children'][$m->ID];
		}
		
		$output['totalChildren'] = count($output['children']);
		$output['childDepth'] = count($output['children']) ? 1 : 0;
		
		foreach ($output['children'] as $child) {
			$output['totalChildren'] += $child['totalChildren'];
			$output['childDepth'] += $child['childDepth'];
		}
		
		return $output;
	}
	
	private function feature_item (&$output) 
	{
		$output .= 
			'<li class="menu-feature-link menu-item menu-item-depth-1">'.
				'<a href="'.esc_url($this->m_item->url).(isExternalLink($this->m_item->url) ? '" target="_blank' : '').'">'.
					esc_attr( $this->m_item->title ).
				'</a>'.
			'</li>';
	}
	
	private function start_complex_el (&$output, $item, $depth = 0, $args = array(), $id = 0) 
	{
		$classes = empty($item->classes) ? [] : (array)$item->classes;
		$classes = apply_filters( 'nav_menu_css_class', $classes, $item, $args );
		
		//$classes[] = 'menu-item-' . $item->ID;
		$classes[] = 'menu-item-depth-'.$depth;
		$classes[] = $args->has_children ? 'dropdown' : null;
		$classes[] = isExternalLink($item->url) ? 'external' : null;
		$classes[] = in_array('current-menu-item', $classes) ? 'active' : null;
		
		$output .= 
			'<li '.assembleAttributes([
				//'id'    => apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args ),
				'class' => implode(' ', array_filter($classes)),
			]).'>';
		
		$attributes = [
			'title'  => empty($item->title) ?  null : $item->title,
			'target' => empty($item->target) ? null : $item->target,
			'rel'    => empty($item->xfn) ?    null : $item->xfn
		];
		
		$linkClasses = [];
		
		$external = isExternalLink($item->url);
		$linkClasses[] = $external ? 'external' : null;
		$attributes = array_merge($attributes, [
			'href'   => empty( $item->url ) ? null : $item->url,
			'target' => $external ? '_blank' : null
		]);
	
		
		// If item has_children make it a dropdown
		if ( $args->has_children && $depth < 2) {
			$linkClasses[] = 'dropdown-toggle';
			$attributes = array_merge($attributes, [
				//'href'          => '#',
				'data-toggle'   => 'dropdown',
			]);
		}
	
		
		$attributes['class'] = implode(' ', $linkClasses);

		$attributes = apply_filters( 'nav_menu_link_attributes', $attributes, $item, $args );

		$item_output = 
			$args->before.
			'<a '. assembleAttributes($attributes) .'>'.
				//(empty( $item->attr_title ) ? '' : '<span class="glyphicon ' . esc_attr( $item->attr_title ) . '"></span>').
				$args->link_before. 
					apply_filters( 'the_title', $item->title, $item->ID ). 
				$args->link_after.
			'</a>'.
			$args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	public function start_lvl( &$output, $depth = 0, $args = array() ) 
	{
		if ($depth == 0) {
			$menuData = $this->m_menuData[$this->m_topLevelItem->ID];
			//$thumbnail = get_the_post_thumbnail_url($this->m_topLevelItem->object_id, 'aether-primary-menu');
			
			if ($menuData['childDepth'] > 1 || $menuData['thumbnail'] !== false) {
				$output .= '<nav role="menu" class="dropdown-menu expanded-panel">';
			}
			else {
				$output .= '<nav role="menu" class="dropdown-menu">';
			}
			
			$output .= '<ul class="persistent-panel-menu">';
		}
		else {
			$output .= '<ul class="third-level-menu">';
		}
	

		if ($depth == 0 && $this->m_item != null) {
			$this->feature_item($output);
		}
	}

	public function end_lvl( &$output, $depth = 0, $args = array() ) 
	{
		$output .= "</ul>";
		if ($depth == 0) {
			$menuData = $this->m_menuData[$this->m_topLevelItem->ID];
			//$thumbnail = get_the_post_thumbnail_url($this->m_topLevelItem->object_id, 'aether-primary-menu');
			
			if ($menuData['childDepth'] > 1 || $menuData['thumbnail'] !== false) {
				$output .= '<aside class="menu-feature-image" style="background-image: url('.$menuData['thumbnail'].')"></aside>';
			}
			
			$output .= '</nav>';
		}
	}

	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )  
	{
		$this->m_item = $item;
		
		if ($depth == 0) {
			$this->m_topLevelItem = $item;
			$this->m_topLevelItemDepth = 0;
		}
		else {
			if ($this->m_topLevelItemDepth < $depth) {
				$this->m_topLevelItemDepth = $depth;
			}
		}
		
		if (($item->attr_title == 'divider' || $item->title == 'divider') && $depth == 1) {
			$output .= '<li role="presentation" class="divider">';
		}
		else if ($item->attr_title == 'dropdown-header' && $depth == 1) {
			$output .= '<li role="presentation" class="dropdown-header">' . esc_attr( $item->title );
		}
		else if ($item->attr_title == 'disabled') {
			$output .= '<li role="presentation" class="disabled"><a href="#">' . esc_attr( $item->title ) . '</a>';
		}
		else {
			$this->start_complex_el($output, $item, $depth, $args, $id);
		}
	}

	public function end_el( &$output, $item, $depth = 0, $args = array() ) 
	{
		$output .= "</li>";
	}
	

	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) 
	{
		if ( ! $element ) {
			return;
		}

		$id_field = $this->db_fields['id'];

		// Display this element.
		if ( is_object( $args[0] ) ) {
			$args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );
		}

		parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}

	public static function fallback( $args ) {
		if ( !current_user_can( 'manage_options' ) ) {
			return;
		}

		/*
		if ( $container ) {
			$attributes = [
				'id'    => empty($args['container_id'])    ? null : $args['container_id'],
				'class' => empty($args['container_class']) ? null : $args['container_class']
			];
			
			echo '<'.$container.' '.assembleAttributes($attributes).'>';
		}
		*/
		
		if ($depth == 0) {
			echo '<nav id="header-nav" class="nav navbar-nav">';
		}

		$attributes = [
			'id'    => empty($args['menu_id'])    ? 'menu-primary-menu' : $args['menu_id'],
			'class' => empty($args['menu_class']) ? 'menu' : $args['menu_class']
		];
		
		echo 
			'<ul '.assembleAttributes($attributes).'>'.
				'<li class="menu-item menu-item-depth-0">'.
					'<a href="' . admin_url( 'nav-menus.php' ) . '">'.__( 'Add a menu', 'aether').'</a>'.
				'</li>'.
			'</ul>';

		if ($depth == 0) {
			echo '</nav>';
		}
		/*
		if ( $container ) {
			echo '</' . $container . '>';
		}
		*/
	}
}


/**
 * End of File
 */
