<?php
/**
 * @package aether
 */
 
namespace Aether;

class FrontpageSplash
{
	static function registerMediaSupport()
	{
		add_theme_support( 'post-thumbnails' );

		add_image_size( 'aether-splash-sd', 1920, 1080, true );
		add_image_size( 'aether-splash-hd', 2560, 1600, true );
	}

	static function registerCustomiser ($wp_customize) 
	{
		// Customiser UI
		$wp_customize->add_section('ae_splash' , [
			'title'      => esc_html__( 'Æ Frontpage Splash', 'aether' ),
			'priority'   => 90,
		]);
		
	
		// Enable
		$wp_customize->add_setting('ae_enable_splash', [
			'default'           => 1,
			'transport'         => 'postMessage',
			'sanitize_callback' => 'Aether\\sanitizeCheckbox'
		]);
		
		$wp_customize->add_control('ae_enable_splash', [
			'type'        => 'checkbox',
			'label'       => __('Enable homepage Splash', 'aether'),
			'description' => __('Display a prominent splash image on your homepage', 'aether'),
			'section'     => 'ae_splash'
		]);
		
	
		// Enable
		$wp_customize->add_setting('ae_splash_title', [
			'default'           => '',
			'transport'         => 'postMessage'
		]);
		
		$wp_customize->add_control('ae_splash_title', [
			'type'        => 'text',
			'label'       => __('Title', 'aether'),
			'section'     => 'ae_splash'
		]);
		
	
		// Enable
		$wp_customize->add_setting('ae_splash_subtitle', [
			'default'           => '',
			'transport'         => 'postMessage'
		]);
		
		$wp_customize->add_control('ae_splash_subtitle', [
			'type'        => 'text',
			'label'       => __('Subtitle', 'aether'),
			'section'     => 'ae_splash'
		]);
		
		
		// Foreground
		$wp_customize->add_setting('ae_splash_foreground', [
			'default'   => '',
			'transport' => 'refresh',
			//'sanitize_callback' => 'ae_sanitize_number'
		]);
		
		$wp_customize->add_control(new \WP_Customize_Media_Control($wp_customize, 'ae_splash_foreground', [
			'label'       => __('Foreground Image', 'aether'),
			'description' => __('Will be scaled down and centered to the middle of your splash area, with the maximum width being the content width.', 'aether'),
			'mime_type'   => 'image',
			'section'     => 'ae_splash'
		]));
		
		
		// Layout
		$wp_customize->add_setting('ae_splash_foreground_size', [
			'default'           => 'm',
			'transport'         => 'refresh'
		]);
		
		$wp_customize->add_control('ae_splash_foreground_size', [
			'type'        => 'select',
			'label'       => __('Foreground Image Size', 'aether'),
			'section'     => 'ae_splash',
			'choices'     => [
				's'    => __('Small / Icon size', 'aether' ),
				'm'   => __('Medium', 'aether' ),
				'l'  => __('Large', 'aether' ),
			]
		]);
		
	
		// Layout
		$wp_customize->add_setting('ae_splash_foreground_position', [
			'default'           => 'center',
			'transport'         => 'refresh'
		]);
		
		$wp_customize->add_control('ae_splash_foreground_position', [
			'type'        => 'select',
			'label'       => __('Foreground Image Position', 'aether'),
			'section'     => 'ae_splash',
			'choices'     => [
				'top'    => __('Top', 'aether' ),
				'center' => __('Middle', 'aether' ),
				'bottom' => __('Bottom', 'aether' )
			]
		]);
		
		// Background
		$wp_customize->add_setting('ae_splash_background', [
			'default'   => '',
			'transport' => 'refresh',
			//'sanitize_callback' => 'ae_sanitize_number'
		]);
		
		$wp_customize->add_control(new \WP_Customize_Media_Control($wp_customize, 'ae_splash_background', [
			'label'       => __( 'Background Image', 'aether' ),
			'description' => __('Will be scaled and cropped to fill the background of your splash area.', 'aether'),
			'mime_type'   => 'image',
			'section'     => 'ae_splash'
		]));
		
		
		// Background Color
		$wp_customize->add_setting('ae_splash_background_color', [
			'default'   => '#343031',
			'transport' => 'refresh',
			//'sanitize_callback' => 'ae_sanitize_number'
		]);
		
		$wp_customize->add_control(new \WP_Customize_Color_Control($wp_customize, 'ae_splash_background_color', [
			'label' => __( 'Background Color', 'aether' ),
			'description'   => __('','aether'),
			'section' => 'ae_splash'
		]));
		
		
		// Font Color
		$wp_customize->add_setting('ae_splash_text_color', [
			'default'           => '#f5f4f4',
			'transport'         => 'refresh',
			//'sanitize_callback' => 'ae_sanitize_number'
		]);
		
		$wp_customize->add_control(new \WP_Customize_Color_Control($wp_customize, 'ae_splash_text_color', [
			'label'         => __('Text Color', 'aether' ),
			'description'   => __('','aether'),
			'section'       => 'ae_splash'
		]));
		
		
		// Shadow Color
		$wp_customize->add_setting('ae_splash_shadow_color', [
			'default'           => '#000000',
			'transport'         => 'refresh',
			//'sanitize_callback' => 'ae_sanitize_number'
		]);
		
		$wp_customize->add_control(new \WP_Customize_Color_Control($wp_customize, 'ae_splash_shadow_color', [
			'label'         => __('Text Shadow Color', 'aether' ),
			'description'   => __('','aether'),
			'section'       => 'ae_splash'
		]));
	}
}

add_action('after_setup_theme',  'Aether\\FrontpageSplash::registerMediaSupport' );
add_action('customize_register', 'Aether\\FrontpageSplash::registerCustomiser');

/**
 * End of File
 */
