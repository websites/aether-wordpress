<?php
/**
 * @package aether
 */
 
namespace Aether;

class PrimaryHeader
{
	static function registerCustomiser ($wp_customize) 
	{
		// Customiser UI
		$wp_customize->add_section('ae_section_header' , [
			'title'      => esc_html__( 'Æ Header & Search', 'aether' ),
			'priority'   => 90,
		]);
	
		// logo
		$wp_customize->add_setting('ae_header_logo', [
			'default'   => '',
			'transport' => 'refresh',
			'sanitize_callback' => 'ae_sanitize_number'
		]);
		
		$wp_customize->add_control(new \WP_Customize_Media_Control($wp_customize, 'ae_header_logo', [
			'label' => __( 'Site Logo', 'aether' ),
			'mime_type' => 'image',
			'section' => 'ae_section_header',
			'priority'  => 10,
		]));

		// Live Search
		$wp_customize->add_setting('ae_option_livesearch', [
			'default'           => 1,
			'transport'         => 'postMessage',
			'sanitize_callback' => 'Aether\\sanitizeCheckbox'
		]);
		
		$wp_customize->add_control('ae_option_livesearch', [
			'type'        => 'checkbox',
			'label'       => __('Enable Live Search', 'aether'),
			'description' => __('Allow users to receive search results as they type. Enabled by default.', 'aether'),
			'section'     => 'ae_section_header'
		]);
		
		// Local Caching
		$wp_customize->add_setting('ae_option_localcaching', [
			'default'           => 1,
			'transport'         => 'postMessage',
			'sanitize_callback' => 'Aether\\sanitizeCheckbox'
		]);
		
		$wp_customize->add_control('ae_option_localcaching', [
			'type'        => 'checkbox',
			'label'       => __('Use local caching for search', 'aether'),
			'description' => __('Stores search results locally for 2 hours on a users machine. Requires Live Search. Enabled by default.', 'aether'),
			'section'     => 'ae_section_header'
		]);
		
		// User Menu
		$wp_customize->add_setting('ae_option_usermenu', [
			'default'           => 0,
			'transport'         => 'postMessage',
			'sanitize_callback' => 'Aether\\sanitizeCheckbox'
		]);
		
		$wp_customize->add_control('ae_option_usermenu', [
			'type'        => 'checkbox',
			'label'       => __('Show user menu', 'aether'),
			'description' => __('Ideal for official KDE websites using LDAP, pointless on small or personal blogs with few users. Disabled by default.', 'aether'),
			'section'     => 'ae_section_header'
		]);
	}
	
	
	/**
	 * Performing a search without admin rights requires the livesearch option to
	 * be enabled. Calls into the standard live search if enabled.
	 */
	public static function ajaxUserSearch () 
	{
		if (\get_theme_mod('ae_option_livesearch', '1') != '1') {
			\wp_send_json([
				'message' => __('Live Search Disabled', 'aether'),
				'cache' => false
			]);
		}
		else {
			self::ajaxPrivSearch();
		}
	}
	
	
	/**
	 * Performs a live search, as provided by the header. This is the same search
	 * provided to users, so no 'admin' results should appear.
	 */
	public static function ajaxPrivSearch () 
	{
		global $wpdb;
		global $post;
		
		$params = struct($_GET, [
			's' => '',
			'page' => 1
		]);

		$results = [];
		
		$query = new \WP_Query([
			's' => $params->s,
			'posts_per_page' => 10,
			'orderby' => [
					'relevance' => 'DESC',
					'post_modified' => 'DESC',
					'post_title' => 'ASC',
			],
		]);
		
		while ($query->have_posts()) {
			$query->the_post();
			
			$results[] = [
				'title' => get_the_title(),
				'author' => get_the_author(),
				'excerpt' => get_the_excerpt(),
				'url' => get_permalink()
			];
		}
		
		\wp_send_json([
			'message' => count($results) == 0 ? __('No results found.', 'aether') : '',
			'totalResults' => count($results),
			'results' => $results,
			'cache' => true
		]);
	}
}

add_action('customize_register',    'Aether\\PrimaryHeader::registerCustomiser');
add_action('wp_ajax_nopriv_search', 'Aether\\PrimaryHeader::ajaxUserSearch');
add_action('wp_ajax_search',        'Aether\\PrimaryHeader::ajaxPrivSearch');

/**
 * End of File
 */
