<?php
/**
 * @package aether
 */
 
namespace Aether;
 
class FooterNavwalker extends \Walker_Nav_Menu 
{
	public function start_lvl( &$output, $depth = 0, $args = array() ) 
	{
	}

	public function end_lvl( &$output, $depth = 0, $args = array() ) 
	{
	}

	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )  
	{
		if ($depth == 0) {
			$output .= '<nav class="col-sm">';
			
			if ($item->url == '#' || $item->url == '') {
				$output .= '<h3>' . esc_attr( $item->title ) . '</h3>';
			}
			else {
				$output .= '<h3><a href="'.esc_url($item->url).(isExternalLink($item->url) ? '" target="_blank' : '').'">' . esc_attr( $item->title ) . '</a></h3>';
			}
		}
		else {
			if ($item->url == '#' || $item->url == '') {
				$output .= '<i>' . esc_attr( $item->title ) . '</i>';
			}
			else {
				$output .= '<a href="'.esc_url($item->url).(isExternalLink($item->url) ? '" target="_blank' : '').'">' . esc_attr( $item->title ) . '</a>';
			}
		}
	}

	public function end_el( &$output, $item, $depth = 0, $args = array() ) 
	{
		if ($depth == 0) {
			$output .= "</nav>";
		}
		//$output .= "</li>";
	}

	public static function fallback( $args )
	{
		if ( !current_user_can( 'manage_options' ) ) {
			return;
		}
		
		if ($depth == 0) {
			echo '<nav class="col-sm">';
		}
		
		echo '<a href="' . admin_url( 'nav-menus.php' ) . '">'.__( 'Add a menu', 'aether').'</a>';

		if ($depth == 0) {
			echo '</nav>';
		}
	}
}


/**
 * End of File
 */
