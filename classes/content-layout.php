<?php
/**
 * @package aether
 */
 
namespace Aether;

function contentLayout() 
{
	static $instance = null;
	
	if ($instance === null) {
		$instance = new ContentLayout();
	}
	
	return $instance;
}

class ContentLayout
{
	private $m_currentLayout;
	private $m_layouts;
	
	public function __construct() 
	{
		$this->m_currentLayout = 'right-sidebar';
		$this->m_layouts = [
			'left-sidebar'  => esc_html__('Left Sidebar','aether'), 
			'right-sidebar' => esc_html__('Right Sidebar','aether'), 
			'no-sidebar'    => esc_html__('No Sidebar','aether'),
			'full-width'    => esc_html__('Full Width', 'aether')
		];
	}
	
	public function layouts() 
	{
		return $this->m_layouts;
	}
	
	public function layout() 
	{
		if ($this->m_currentLayout == null) {
			return 'right-sidebar';
		}
		else {
			return $this->m_currentLayout;
		}
	}
	
	public function setLayout($layout) 
	{
		if (isset($this->m_layouts[$layout])) {
			$this->m_currentLayout = $layout;
			return true;
		}
		else {
			return false;
		}
	}
}


/**
 * End of File
 */
