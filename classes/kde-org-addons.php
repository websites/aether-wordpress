<?php
/**
 * @package aether
 */
 
namespace Aether;

class KdeOrgAddons
{
	static function registerCustomiser ($wp_customize) 
	{
		// Customiser UI
		$wp_customize->add_section('ae_kdeorg' , [
			'title'      => esc_html__( 'Æ KDE.org Branding', 'aether' ),
			'priority'   => 90,
		]);
		
	
	
		$wp_customize->add_setting('ae_enable_kdeorg_logo', [
			'default'           => 1,
			'transport'         => 'refresh',
			'sanitize_callback' => 'Aether\\sanitizeCheckbox'
		]);
		
		$wp_customize->add_control('ae_enable_kdeorg_logo', [
			'type'        => 'checkbox',
			'label'       => __('Use the KDE Logo', 'aether'),
			'description' => __('Use the KDE logo in the header. If using a custom site icon, that icon will be used instead.', 'aether'),
			'section'     => 'ae_kdeorg'
		]);
		
	
		$wp_customize->add_setting('ae_enable_kdeorg_patrons', [
			'default'           => 0,
			'transport'         => 'refresh',
			'sanitize_callback' => 'Aether\\sanitizeCheckbox'
		]);
		
		$wp_customize->add_control('ae_enable_kdeorg_patrons', [
			'type'        => 'checkbox',
			'label'       => __('Show KDE Patrons', 'aether'),
			'description' => __('Will toggle the display of patrons, personal blogs should not show patrons.', 'aether'),
			'section'     => 'ae_kdeorg'
		]);
		
		$wp_customize->add_setting('ae_enable_kdeorg_income', [
			'default'           => 1,
			'transport'         => 'refresh',
			'sanitize_callback' => 'Aether\\sanitizeCheckbox'
		]);
		
		$wp_customize->add_control('ae_enable_kdeorg_income', [
			'type'        => 'checkbox',
			'label'       => __('Show KDE donate and merch links', 'aether'),
			'description' => __('Will toggle the display of income sources.', 'aether'),
			'section'     => 'ae_kdeorg'
		]);
	
		$wp_customize->add_setting('ae_enable_kdeorg_social', [
			'default'           => 1,
			'transport'         => 'refresh',
			'sanitize_callback' => 'Aether\\sanitizeCheckbox'
		]);
		
		$wp_customize->add_control('ae_enable_kdeorg_social', [
			'type'        => 'checkbox',
			'label'       => __('Show KDE Social Links', 'aether'),
			'description' => __('Will toggle the display of KDE social links.', 'aether'),
			'section'     => 'ae_kdeorg'
		]);
		
		$wp_customize->add_setting('ae_enable_kdeorg_legal', [
			'default'           => 0,
			'transport'         => 'refresh',
			'sanitize_callback' => 'Aether\\sanitizeCheckbox'
		]);
		
		$wp_customize->add_control('ae_enable_kdeorg_legal', [
			'type'        => 'checkbox',
			'label'       => __('Show KDE legal notices in the footer', 'aether'),
			'description' => __('Includes copyrights and links.', 'aether'),
			'section'     => 'ae_kdeorg'
		]);
	}
}

add_action('customize_register', 'Aether\\KdeOrgAddons::registerCustomiser');

/**
 * End of File
 */
