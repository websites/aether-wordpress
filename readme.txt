/*========= About Theme =========*/

Original Theme Name: Activello
Fork Theme Name: aether
Original Theme URI: http://colorlib.com/wp/Activello/
Version: 1.0.0
Tested up to: WP 4.4

Original Author: Aigars Silkalns
Fork Author: Alejandro Moreno
Original Author URI: http://colorlib.com/wp/
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl.html
-------------------------------------------------------
Activello theme, Copyright 2016 colorlib.com
Activello WordPress theme is distributed under the terms of the GNU GPL
Activello is based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc.
-------------------------------------------------------
aether Wordpress theme is distributed under the terms of the GNU GPL
aether is a fork based on Activello WordPress theme (v1.0.2) and Underscores
-------------------------------------------------------

/*========= Credits =========*/
Unless otherwise specified, all the theme files, scripts and images are licensed under GPLv2 license

aether theme uses:
* Activello WordPress Theme as the basis of the fork (http://colorlib.com/wp/Activello/) licensed under the GNU General Public License v3.0.
* FontAwesome (http://fontawesome.io) licensed under the SIL OFL 1.1 (http://scripts.sil.org/OFL)
* Bootstrap and GLYPHICONS Halflings (http://getbootstrap.com/) licensed under MIT license (https://github.com/twbs/bootstrap/blob/master/LICENSE)
* WP-Bootstrap-NavWalker licensed under the GPLv2 license (http://www.gnu.org/licenses/gpl-2.0.html)
* FlexSlider by WooThemes licensed under the GPLv2 license (http://www.gnu.org/licenses/gpl-2.0.html)
* Modernizr (https://github.com/Modernizr/Modernizr) licensed under MIT license
* Unless otherwise specified, all images are of kde


/*========= Description =========*/

aether IS A FORK OF ACTIVELLO (v1.0.2) FOR WORDPRESS WEB PAGES RELATED TO THE KDE ECOSYSTEM


/*========= Installation =========*/


You can download the theme and put in the "wp-content/themes" folder of your WordPress installation.


/*========= Theme Features =========*/

* Bootstrap 3 integration
* Responsive design
* Unlimited color variations
* SEO friendly
* WordPress Customizer
* Image centric approach
* Internationalized & localization
* Drop-down Menu
* Cross-browser compatibility
* Threaded Comments
* Gravatar ready
* Featured slider
* Font Awesome icons

----------------------------------
"FREE SOFTWARE FOR A BETTER WORLD"
----------------------------------
