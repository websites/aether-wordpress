<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package aether
 */
 
	wp_enqueue_style( 'search-noresults', get_template_directory_uri() . '/css/search-noresults.css' );
	get_template_part( 'parts/content-start' );
	
?>

<main class="<?php ae_main_classes(); ?>">
	<section class="no-results not-found">
		<img class="search-icon" src="<?php echo get_template_directory_uri() ?>/media/folder.svg" />
		<p><?php esc_html_e( 'There is nothing in this archive.', 'aether' ); ?></p>
	</section>
</main>

<?php 
	get_template_part( 'parts/content-end' );
?>
