<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package aether
 */
 
	ae_use_custom_content_bookends();
	get_header(); 

	if (have_posts()) {
			get_template_part( 'search-resultlist' );
	}
	else {
			get_template_part( 'search-noresults' );
	}

	get_footer();
