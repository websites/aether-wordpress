<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package aether
 */
	
	if (!defined('AE_USE_CUSTOM_BOOKENDS')) {
		echo '</main>';
		get_template_part( 'parts/content-end' );
	}
?>
	<footer id="kFooter" class="footer">

		<?php if(\get_theme_mod('ae_enable_kdeorg_patrons', '1') == '1'): ?>
		<section id="kPatrons" class="container">
			<h3>Patrons</h3>
			<div id="patrons-list">
				<figure><img src="<?= get_template_directory_uri() ?>/media/patrons/canonical.svg" /><figcaption>Canonical</figcaption></figure>
				<figure><img src="<?= get_template_directory_uri() ?>/media/patrons/google.svg" /><figcaption>Google</figcaption></figure>
				<figure><img src="<?= get_template_directory_uri() ?>/media/patrons/suse.svg" /><figcaption>SUSE</figcaption></figure>
				<figure><img src="<?= get_template_directory_uri() ?>/media/patrons/qt-company.svg" /><figcaption>The Qt Company</figcaption></figure>
				<figure><img src="<?= get_template_directory_uri() ?>/media/patrons/blue-systems.svg" /><figcaption>Blue Systems</figcaption></figure>
				<figure><img src="<?= get_template_directory_uri() ?>/media/patrons/privateinternetaccess.svg" /><figcaption>Private Internet Access</figcaption></figure>
			</div>
		</section>
		<?php endif; ?>
		
		<?php if(\get_theme_mod('ae_enable_kdeorg_income', '1') == '1'): ?>
		<section id="kFooterIncome" class="container">
			<div id="kDonateForm">
				<div class="center">
					<h3>Donate to KDE <a href="https://www.kde.org/community/donations/index.php#money" target="_blank">Why Donate?</a></h3>
					<form 
						action="https://www.paypal.com/en_US/cgi-bin/webscr" 
						method="post" 
						onsubmit="return amount.value >= 2 || window.confirm('Your donation is smaller than 2€. This means that most of your donation\nwill end up in processing fees. Do you want to continue?');">
							<input type="hidden" name="cmd" value="_donations" />
							<input type="hidden" name="lc" value="GB" />
							<input type="hidden" name="item_name" value="Development and communication of KDE software" />
							<input type="hidden" name="custom" value="<?= 
								'//'.
								$_SERVER['HTTP_HOST'].
								$_SERVER['REQUEST_URI'].
								'/donation_box'
								?>" />
							<input type="hidden" name="currency_code" value="EUR" />
							<input type="hidden" name="cbt" value="Return to www.kde.org" />
							<input type="hidden" name="return" value="https://www.kde.org/community/donations/thanks_paypal.php" />
							<input type="hidden" name="notify_url" value="https://www.kde.org/community/donations/notify.php" />
							<input type="hidden" name="business" value="kde-ev-paypal@kde.org" />
							<input type='text' name="amount" value="10.00" id="donateAmountField" /> €
							<button type='submit' id="donateSubmit">Donate via PayPal</button>
					</form>
					
					<a href="https://www.kde.org/community/donations/others.php" id="otherWaysDonate" target="_blank">Other ways to donate</a>
				</div>
			</div>
			<div id="kMetaStore">
				<div class="center">
					<h3>Visit the KDE MetaStore</h3>
					<p>Show your love for KDE! Purchase books, mugs, apparel, and more to support KDE.</p>
					<a href="https://www.kde.org/stuff/metastore.php" class="button">Click here to browse</a>
				</div>
			</div>
		</section>
		<?php endif; ?>
		
		<?php if (has_nav_menu('footer') || current_user_can( 'manage_options' )): ?>
		<section id="kLinks" class="container">
			<?php 
			wp_nav_menu([
				//'container_id'      => 'footer-nav',
				'menu'              => 'footer',
				'items_wrap' => '%3$s',
				//'theme_location'    => 'footer',
				'depth'             => 2,
				'container'         => 'div',
				'container_class'   => 'row',
				//'menu_class'        => 'nav navbar-nav',
				'fallback_cb'       => 'Aether\\PrimaryNavwalker::fallback',
				'walker'            => new Aether\FooterNavwalker()
			]);
			
		?>
		</section>
		<?php endif; ?>
	
		<?php if(\get_theme_mod('ae_enable_kdeorg_social', '1') == '1'): ?>
		<section id="kSocial" class="container kSocialLinks">
			<a class="shareFacebook" href="https://www.facebook.com/kde/" rel="nofollow">Post on Facebook</a>
			<a class="shareTwitter" href="https://twitter.com/kdecommunity" rel="nofollow">Share on Twitter</a>
			<a class="shareGoogle" href="https://plus.google.com/105126786256705328374/posts" rel="nofollow">Social Link 1</a>
		</section>
		<?php endif; ?>
	
		<?php if(\get_theme_mod('ae_enable_kdeorg_legal', '1') == '1'): ?>
		<section id="kLegal" class="container">
			<div class="row">
				<small class="col-8" style="text-align: right;">
					KDE<sup>&#174;</sup> and <a href="media/images/trademark_kde_gear_black_logo.png">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href="https://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> |
					<a href="https://www.kde.org/community/whatiskde/impressum.php">Legal</a> |
					<a href="<?= admin_url() ?>">Admin</a>
				</small>
			</div>
		</section>
		<?php else: ?>
		<section id="kLegal" class="container">
			<div class="row">
				<small class="col-8" style="text-align: center;">
					<a href="<?= admin_url() ?>">Admin</a>
				</small>
			</div>
		</section>
		<?php endif; ?>
	</footer>

<?php wp_footer(); 

if (current_user_can('administrator') && 0){
    global $wpdb;
    echo "<pre>";
    print_r($wpdb->queries);
    echo "</pre>";
}

?>



</body>
</html>
