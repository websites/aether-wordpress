<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package aether
 */

$show_sidebar = true;
if( is_singular() && ( get_post_meta($post->ID, 'site_layout', true) ) ) 
{
	if( get_post_meta($post->ID, 'site_layout', true) == 'no-sidebar' || get_post_meta($post->ID, 'site_layout', true) == 'full-width' ) {
	$show_sidebar = false;               
	}
	}
	elseif( get_theme_mod( 'ae_sidebar_position' ) == "no-sidebar" ||  get_theme_mod( 'ae_sidebar_position' ) == "full-width" ) {
	$show_sidebar = false;
}

if( $show_sidebar ):
?>
<aside class="primary-sidebar">
	<?php 
	do_action( 'before_sidebar' ); 
	
	if ( ! dynamic_sidebar( 'ae-sidebar' ) ) :
	?>
		<aside id="sidebar-error" class="widget">
			<h3 class="widget-title"><?php esc_html_e( 'No Sidebar', 'aether' ); ?></h3>
			<ul>
				<?php _e('No sidebar content found.', 'aether'); ?>
			</ul>
		</aside>

	<?php 
	endif; // end sidebar widget area 
	?>
</aside>
<?php 
endif; 
?>
