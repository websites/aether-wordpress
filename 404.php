<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package aether
 */
 
wp_enqueue_style( 'error-404', get_template_directory_uri() . '/css/error-404.css' );

ae_use_custom_content_bookends();
get_header(); 
get_template_part( 'parts/content-start' );


?>
<main class="<?php ae_main_classes(); ?>">
	<section class="error-404 not-found">
		<img class="error-icon" src="<?php echo get_template_directory_uri() ?>/media/x.svg" />
		<header class="page-header">
			<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'aether' ); ?></h1>
		</header><!-- .page-header -->

		<div class="page-content">
			<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'aether' ); ?></p>


			<div class="row">
				<div class="col-md-6 not-found-widget">
					<?php the_widget( 'WP_Widget_Recent_Posts', 'title='.esc_html__( 'Recent Posts', 'aether' ) ); ?>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 not-found-widget">
					<?php
					/* translators: %1$s: smiley */
					$archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'aether' ), convert_smilies( ':)' ) ) . '</p>';
					the_widget( 'WP_Widget_Archives', 'dropdown=1&title='.esc_html__( 'Archives', 'aether' ), "after_title=</h2>$archive_content" );
					?>
				</div>

				<div class="col-md-6 not-found-widget">
					<?php the_widget( 'WP_Widget_Tag_Cloud', 'title='.esc_html__( 'Tags', 'aether' ) ); ?>
				</div>
			</div>
	</section><!-- .error-404 -->
</main>
<?php 



get_template_part( 'parts/content-end' );
get_footer(); 
