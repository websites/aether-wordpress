<?php
/**
 * @package aether
 */
 
	ae_use_custom_content_bookends();
	get_header();
 
?>

<header class="primary-header-block<?= has_post_thumbnail() ? ' image-background' : '' ?>"<?= has_post_thumbnail() ? ' style="background-image: url('.get_the_post_thumbnail_url( null, 'single-featured').')"' : '' ?>>
	<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
	<?php ae_posted_on(); ?>
</header>

<?php 

	get_template_part( 'parts/content-start' );

?>

<main class="<?php ae_main_classes(); ?>">
<section class="page-single">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		
		
			<div class="blog-item-wrap">
				<div class="post-inner-content">

								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
												<?php the_post_thumbnail( 'aether-featured', array( 'class' => 'single-featured' )); ?>
								</a>

					<?php if ( is_search() ) : // Only display Excerpts for Search ?>
					<div class="entry-summary">
						<?php the_excerpt(); ?>
						<p><a class="btn btn-default read-more" href="<?php the_permalink(); ?>"><?php esc_html_e( 'Read More', 'aether' ); ?></a></p>
					</div><!-- .entry-summary -->
					<?php else : ?>
					<div class="entry-content">

						<?php
						//if ( get_the_excerpt() != "" ) :
					//		the_excerpt();
					//	else :
							the_content();
					//	endif;
						?>



						<?php if( ! is_single() ) : ?>
						<div class="read-more">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php esc_html_e( 'Read More', 'aether' ); ?></a>
						</div>
						<?php endif; ?>

																		<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
																				<div class="entry-footer">
							<span class="comments-link"><?php comments_popup_link( esc_html__( 'Leave a comment', 'aether' ), esc_html__( '1 Comment', 'aether' ), esc_html__( '% Comments', 'aether' ) ); ?></span>
																				</div><!-- .entry-footer -->
																		<?php endif; ?>
					</div><!-- .entry-content -->
					<?php endif; ?>
				</div>
			</div>
		</article><!-- #post-## -->
		
		</section>
<?php
 
	// End the loop.

	get_template_part( 'parts/content-end' );
	get_footer();
