<?php
/**
 * @package aether
 */
 
	ae_use_custom_content_bookends();
	get_header();
 
?>

<header class="primary-header-block<?= has_post_thumbnail() ? ' image-background' : '' ?>"<?= has_post_thumbnail() ? ' style="background-image: url('.get_the_post_thumbnail_url( null, 'single-featured').')"' : '' ?>>
	<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
</header>

<?php 

	get_template_part( 'parts/content-start' );

?>

<main class="<?php ae_main_classes(); ?>">

<?php 

	while ( have_posts() ) : 
	the_post();

?>

	<section class="page-single">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content">
				<?php
					the_content();
				?>
			</div>
		</article>
	</section>
		
<?php 

	if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) {
		comment_form();
	}

	endwhile;

?>
		
</main>
<?php
 
	// End the loop.

	get_template_part( 'parts/content-end' );
	get_footer();

