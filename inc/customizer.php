<?php
/**
 * aether Theme Customizer
 *
 * @package aether
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function ae_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'ae_customize_register' );

/**
 * Options for WordPress Theme Customizer.
 */
function ae_customizer( $wp_customize ) {

	// logo
	$wp_customize->add_setting( 'header_logo', array(
		'default' => '',
		'transport'   => 'refresh',
                'sanitize_callback' => 'ae_sanitize_number'
	) );
        $wp_customize->add_control(new WP_Customize_Media_Control( $wp_customize, 'header_logo', array(
    		'label' => __( 'Logo', 'aether' ),
    		'section' => 'title_tagline',
    		'mime_type' => 'image',
    		'priority'  => 10,
    	) ) );


    global $header_show;
    $wp_customize->add_setting('header_show', array(
            'default' => 'logo-text',
            'sanitize_callback' => 'ae_sanitize_radio_header'
        ));
        $wp_customize->add_control('header_show', array(
            'type' => 'radio',
            'label' => __('Show', 'aether'),
            'section' => 'title_tagline',
            'choices' => $header_show
        ));

        /* Main option Settings Panel */
    $wp_customize->add_panel('ae_main_options', array(
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => __('Æther Options', 'aether'),
        'description' => __('Panel to update aether theme options', 'aether'), // Include html tags such as <p>.
        'priority' => 10 // Mixed with top-level-section hierarchy.
    ));

	// add "Content Options" section
	$wp_customize->add_section( 'ae_content_section' , array(
		'title'      => esc_html__( 'Content Options', 'aether' ),
		'priority'   => 50,
                'panel' => 'ae_main_options'
	) );

	// add setting for excerpts/full posts toggle
	$wp_customize->add_setting( 'ae_excerpts', array(
		'default'           => 1,
		'sanitize_callback' => 'ae_sanitize_checkbox',
	) );

	// add checkbox control for excerpts/full posts toggle
	$wp_customize->add_control( 'ae_excerpts', array(
		'label'     => esc_html__( 'Show post excerpts?', 'aether' ),
		'section'   => 'ae_content_section',
		'priority'  => 10,
		'type'      => 'checkbox'
	) );

	$wp_customize->add_setting( 'ae_page_comments', array(
		'default' => 1,
		'sanitize_callback' => 'ae_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'ae_page_comments', array(
		'label'		=> esc_html__( 'Display Comments on Static Pages?', 'aether' ),
		'section'	=> 'ae_content_section',
		'priority'	=> 20,
		'type'      => 'checkbox',
	) );


	// add "Featured Posts" section
	$wp_customize->add_section( 'ae_featured_section' , array(
		'title'      => esc_html__( 'Slider Option', 'aether' ),
		'priority'   => 60,
                'panel' => 'ae_main_options'
	) );

	$wp_customize->add_setting( 'ae_featured_cat', array(
		'default' => 0,
		'transport'   => 'refresh',
                'sanitize_callback' => 'ae_sanitize_slidecat'
	) );

	/*
	$wp_customize->add_control( 'ae_featured_cat', array(
		'type' => 'select',
		'label' => 'Choose a category',
		'choices' => ae_cats(),
		'section' => 'ae_featured_section',
	) );
	*/

	$wp_customize->add_setting( 'ae_featured_hide', array(
		'default' => 0,
		'transport'   => 'refresh',
                'sanitize_callback' => 'ae_sanitize_checkbox'
	) );

	$wp_customize->add_control( 'ae_featured_hide', array(
		'type' => 'checkbox',
		'label' => 'Show Slider',
		'section' => 'ae_featured_section',
	) );


	// add "Sidebar" section
        $wp_customize->add_section('ae_layout_section', array(
            'title' => __('Layout options', 'aether'),
            'priority' => 31,
            'panel' => 'ae_main_options'
        ));
            // Layout options
            $wp_customize->add_setting('ae_sidebar_position', array(
                 'default' => 'side-right',
            ));
            $wp_customize->add_control('ae_sidebar_position', array(
                 'label' => __('Website Layout Options', 'aether'),
                 'section' => 'ae_layout_section',
                 'type'    => 'select',
                 'description' => __('Choose between different layout options to be used as default', 'aether'),
                 'choices'    => Aether\contentLayout()->layouts()
            ));

            $wp_customize->add_setting('accent_color', array(
                    'default' => '',
                    'sanitize_callback' => 'ae_sanitize_hexcolor'
                ));
            $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'accent_color', array(
                'label' => __('Accent Color', 'aether'),
                'description'   => __('Default used if no color is selected','aether'),
                'section' => 'ae_layout_section',
            )));

            $wp_customize->add_setting('social_color', array(
                'default' => '',
                'sanitize_callback' => 'ae_sanitize_hexcolor'
            ));
            $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'social_color', array(
                'label' => __('Social icon color', 'aether'),
                'description' => sprintf(__('Default used if no color is selected', 'aether')),
                'section' => 'ae_layout_section',
            )));

            $wp_customize->add_setting('social_hover_color', array(
                'default' => '',
                'sanitize_callback' => 'ae_sanitize_hexcolor'
            ));
            $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'social_hover_color', array(
                'label' => __('Social Icon:hover Color', 'aether'),
                'description' => sprintf(__('Default used if no color is selected', 'aether')),
                'section' => 'ae_layout_section',
            )));

	// add "Footer" section
	$wp_customize->add_section( 'ae_footer_section' , array(
		'title'      => esc_html__( 'Footer', 'aether' ),
		'priority'   => 90,
	) );

	$wp_customize->add_setting( 'ae_footer_copyright', array(
		'default' => '',
		'transport'   => 'refresh',
                'sanitize_callback' => 'wp_kses_stripslashes'
	) );

	$wp_customize->add_control( 'ae_footer_copyright', array(
		'type' => 'textarea',
		'label' => 'Copyright Text',
		'section' => 'ae_footer_section',
	) );

        /* aether Other Options */
        $wp_customize->add_section('ae_other_options', array(
            'title' => __('Other', 'aether'),
            'priority' => 70,
            'panel' => 'ae_main_options'
        ));
            $wp_customize->add_setting('custom_css', array(
                'default' => '',
                'sanitize_callback' => 'wp_kses_stripslashes'
            ));
            $wp_customize->add_control('custom_css', array(
                'label' => __('Custom CSS', 'aether'),
                'description' => sprintf(__('Additional CSS', 'aether')),
                'section' => 'ae_other_options',
                'type' => 'textarea'
            ));

}
add_action( 'customize_register', 'ae_customizer' );


/**
 * Sanitzie checkbox for WordPress customizer
 */
function ae_sanitize_checkbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}

/**
 * Adds sanitization callback function: colors
 * @package aether
 */
function ae_sanitize_hexcolor($color) {
    if ($unhashed = sanitize_hex_color_no_hash($color))
        return '#' . $unhashed;
    return $color;
}

/**
 * Adds sanitization callback function: Slider Category
 * @package aether
 */
function ae_sanitize_slidecat( $input ) {

/*
    if ( array_key_exists( $input, ae_cats()) ) {
        return $input;
    } else {
        return '';
    }
    */
    
    return $input;
}

/**
 * Adds sanitization callback function: Radio Header
 * @package aether
 */
function ae_sanitize_radio_header( $input ) {
   global $header_show;
    if ( array_key_exists( $input, $header_show ) ) {
        return $input;
    } else {
        return '';
    }
}

/**
 * Adds sanitization callback function: Number
 * @package aether
 */
function ae_sanitize_number($input) {
    if ( isset( $input ) && is_numeric( $input ) ) {
        return $input;
    }
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function ae_customize_preview_js() {
	wp_enqueue_script( 'ae_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20160217', true );
}
add_action( 'customize_preview_init', 'ae_customize_preview_js' );

/**
 * Add CSS for custom controls
 */
function ae_customizer_custom_control_css() {
	?>
    <style>
        #customize-control-aether-main_body_typography-size select, #customize-control-aether-main_body_typography-face select,#customize-control-aether-main_body_typography-style select { width: 60%; }
    </style><?php
}
add_action( 'customize_controls_print_styles', 'ae_customizer_custom_control_css' );
