<?php
/**
 * @package aether
 */
 
if(is_active_sidebar('ae-sidebar')) {
	wp_enqueue_style( 'sidebar', get_template_directory_uri() . '/css/sidebar.css' );
	echo '<div class="content-sidebar-container container">';
}
