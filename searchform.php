<?php
/**
 * The template for displaying search forms in aether
 *
 * @package aether
 */
 
 wp_enqueue_style( 'search-form', get_template_directory_uri() . '/css/search-form.css' );
 
 global $wp_query;
 
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<?php
	
	if (is_search()) {
		echo '<p class="search-resultcount">';
		echo $wp_query->found_posts == 0 ? __('No results found','aether') : sprintf(__('%d results found','aether'), $wp_query->found_posts);
		echo '</p>';
	}
	
	?>
	<div class="input-group">
		<input type="text" class="form-control search-query" placeholder="<?php echo esc_attr_x( 'Search&hellip;', 'placeholder', 'aether' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'aether' ); ?>" />
		<span class="input-group-btn">
			<button type="submit" class="btn btn-default" name="submit" id="searchsubmit" value="<?php echo _e( 'Search', 'aether' ); ?>"><?php echo esc_attr_x( 'Search', 'submit button', 'aether' ); ?></button>
		</span>
	</div>
</form>
