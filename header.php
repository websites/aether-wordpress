<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package aether
 */
 
	$show_logo = true;
	$show_title = false;
	$show_tagline = true;
	$logo = get_theme_mod('header_logo', '');
	$header_show = get_theme_mod('header_show', 'logo-text');

	if( $header_show == 'logo-only' ){
		$show_tagline = false;
	}
	elseif( $header_show == 'title-only' ){
		$show_tagline = $show_logo = false;
	}
	elseif( $header_show == 'title-text' ){
		$show_logo = false;
		$show_title = true;
	}

	function get_custom_body_classes() {
		$classes = get_body_class();

		if (array_search('custom-background', $classes) !== false) {
			unset($classes[array_search('custom-background', $classes)]);
		}

		return 'class="'.implode(' ', $classes).'"';
	}
 
	$custom_icon = get_option( 'site_icon', false ) ? true : false;
	$logo_letter = \get_theme_mod('ae_enable_kdeorg_logo', '1') == '1' ? 'k' : 'w';
 
?><!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" sizes="16x16" type="image/png" href="<?= get_template_directory_uri().'/media/16x16.svg'; ?>" />
	<link rel="shortcut icon" sizes="24x24" type="image/png" href="<?= get_template_directory_uri().'/media/24x24.svg'; ?>" />
	<link rel="shortcut icon" sizes="192x192" type="image/png" href="<?= get_template_directory_uri().'/media/192x192.png'; ?>" />
	<link rel="shortcut icon" sizes="180x180" type="image/png" href="<?= get_template_directory_uri().'/media/180x180.png'; ?>" />
	<link rel="shortcut icon" sizes="128x128" type="image/png" href="<?= get_template_directory_uri().'/media/128x128.svg'; ?>" />
	<link rel="shortcut icon" sizes="64x64" type="image/png" href="<?= get_template_directory_uri().'/media/64x64.svg'; ?>" />
	<link rel="shortcut icon" sizes="32x32" type="image/png" href="<?= get_template_directory_uri().'/media/32x32.svg'; ?>" />
	<?php wp_head(); ?>
	<script>aetherJsConfig=<?=
	
	json_encode([
		'url' => admin_url('admin-ajax.php'),
		'locale' => get_locale(),
		'localCache' => \get_theme_mod('ae_option_livesearch', '1') == '1' ? true : false
	]);
	
	/*{'url': '<?= str_replace("'","\\'",admin_url('admin-ajax.php')) ?>', 'locale': '<?= get_locale() ?>'};*/
	?></script>
</head>

<body <?= get_custom_body_classes(); ?>>
<header id="primary-header" class="navbar navbar-default">
	<div id="header-container" class="center-div container">
	<?php
	
	?>
		<h1 id="header-logo" class="logo-letter-<?= $logo_letter ?> navbar-header menu-item-depth-0<?= is_home() ? ' active' : '' ?><?= $custom_icon ? ' custom-icon' : '' ?>"><a href="<?= esc_url( home_url( '/' ) ); ?>"><?= 
		$custom_icon ? '<img src="'.get_site_icon_url(60).'" id="header-icon" alt="'.get_bloginfo('name').'" />' : get_bloginfo('name')
		?>
		</a></h1><button id="header-menu-button" type="button"><?php echo _e( 'Show Menu', 'aether' ); ?></button><?php 

			wp_nav_menu([
				'container_id'      => 'header-nav',
				'menu'              => 'primary',
				'theme_location'    => 'primary',
				'depth'             => 3,
				'container'         => 'nav',
				'container_class'   => 'nav navbar-nav',
				//'menu_class'        => 'nav navbar-nav',
				'fallback_cb'       => 'Aether\\PrimaryNavwalker::fallback',
				'walker'            => new Aether\PrimaryNavwalker('primary')
			]);
			
		?>
		
		<button id="header-search-mode-toggle" class="monochrome-button" type="button"><?php echo _e( 'Show Search', 'aether' ); ?></button>
		
	</div>
	
		<div id="header-search-container">
			<form id="header-search-form" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<input  id="header-search-input"  type="text"  placeholder="<?php echo esc_attr_x( 'Search&hellip;', 'placeholder', 'aether' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'aether' ); ?>" autocomplete="off" />
					
					<button id="header-search-submit" type="submit" disabled="disabled"><?php echo _e( 'Submit', 'aether' ); ?></button>
					<article id="header-search-results"></article>
			</form>
		</div>
		
</header>


 <aside id="cookieWarning">
	<p>This website uses cookies to remember your settings, retain your sessions, and learn about our site usage. If your browser is set to &quot;Do not track&quot; mode we will not use statistics cookies.</p>
	<button id="removecookie">Dismiss</button>
</aside>

 <?php if(0):?>
	<nav>
		<div>
			<?php 
			if(\get_theme_mod('ae_option_usermenu', '1') == '1' || is_customize_preview()):
				echo '<button id="header-user-button" class="monochrome-button glyph-user'.(is_user_logged_in() ? ' user-signed-in' : '').'" type="button" style="';
					echo (\get_theme_mod('ae_option_usermenu', '1') != '1' ? ' display: none;' : '');
					echo '">';
						if (is_user_logged_in()):
							echo '<img src="'.get_avatar_url(get_current_user_id()).'" />';
						endif;
					echo wp_get_current_user()->display_name;
				echo '</button>';
			endif;
			
			if (!is_customize_preview() && current_user_can('manage_options')):
				$commentCount = get_comment_count();
				
				echo '<a href="'.admin_url('edit-comments.php').'" class="monochrome-button glyph-comment" type="button" style="';
					echo '">';
					if ($commentCount['awaiting_moderation'] > 0):
						echo '<i>'.$commentCount['awaiting_moderation'].'</i>';
					endif;
					echo __( 'Comments', 'aether' );
				echo '</a>';
				
				
			//if(\get_theme_mod('ae_option_usermenu', '1') == '1' || is_customize_preview()):
				echo '<a href="'.admin_url('post-new.php?post_type=page').'" class="monochrome-button glyph-page-new" type="button" style="';
					echo '">';
					echo __( 'New Page', 'aether' );
				echo '</a>';
			//endif;
				
			//if(\get_theme_mod('ae_option_usermenu', '1') == '1' || is_customize_preview()):
				echo '<a href="'.admin_url('post-new.php').'" class="monochrome-button glyph-news-new" type="button" style="';
					echo '">';
					echo __( 'New Post', 'aether' );
				echo '</a>';
			//endif;
			
			//if(\get_theme_mod('ae_option_usermenu', '1') == '1' || is_customize_preview()):
				echo '<a href="'.get_edit_post_link().'" id="header-admin-button" class="monochrome-button glyph-edit" type="button" style="';
					echo '">';
					echo __( 'Edit', 'aether' );
				echo '</a>';
			//endif;
			
			//admin_url('post-new.php')
			
			//if(\get_theme_mod('ae_option_usermenu', '1') == '1' || is_customize_preview()):
				echo '<a href="'.admin_url().'" class="monochrome-button glyph-wordpress" type="button" style="';
					echo '">';
					echo __( 'Wordpress Admin Panel', 'aether' );
				echo '</a>';
			endif;
			?>
		</div>
	</nav>
 <?php endif; ?>
<?php

	function printForegroundImage ($pos) {
		if ($pos == \get_theme_mod('ae_splash_foreground_position', 'center')) {
			$url = \wp_get_attachment_image_url(\get_theme_mod('ae_splash_foreground'), 'aether-splash-sd');
			$class = 'splash-fg-'.\get_theme_mod('ae_splash_foreground_size', 'm');
			
			echo '<img src="'.$url.'" class="'.$class.'" />';
		}
	}

	if (is_front_page() && (\get_theme_mod('ae_enable_splash', '1') == '1' || is_customize_preview())) :

	
	$shadow = \get_theme_mod('ae_splash_shadow_color', '#000000');
	list($r, $g, $b) = sscanf($shadow, "#%02x%02x%02x");
	
?>
<style>
	#header-splash-background { 
		color: <?php echo \get_theme_mod('ae_splash_text_color', '#f5f4f4'); ?>;
		background-color: <?php echo \get_theme_mod('ae_splash_background_color', '#343031'); ?>;
		text-shadow: 0px 1px 3px <?php echo "rgba( $r,$g,$b,1)" ?>;
		background-image: url(<?php echo \wp_get_attachment_image_url(\get_theme_mod('ae_splash_background'), 'aether-splash-sd'); ?>);
	}
	#header-splash-background { color: <?php echo \get_theme_mod('ae_splash_text_color', '#f5f4f4');?> }
</style>
<?php
	echo '<section id="header-splash-background" style="'.
		(\get_theme_mod('ae_splash_background', '') != '' ? 'background-image: url(\''.\wp_get_attachment_image_url(\get_theme_mod('ae_splash_background'), 'aether-splash-sd').'\');' : '').
		('color: '.\get_theme_mod('ae_splash_text_color', '#f5f4f4').';').
		('background-color: '.\get_theme_mod('ae_splash_background_color', '#343031').';').
	'">';
	
		printForegroundImage('top');
	
		if (get_theme_mod('ae_splash_title', '') != '') {
			echo '<h1>'.htmlspecialchars(\get_theme_mod('ae_splash_title', '')).'<h1>';
		}
	
		printForegroundImage('center');
		
		if (get_theme_mod('ae_splash_subtitle', '') != '') {
			echo '<h2>'.htmlspecialchars(\get_theme_mod('ae_splash_subtitle', '')).'<h2>';
		}
		
		printForegroundImage('bottom');
		
		/*echo '<div id="header-splash-container" class="container" style="'.
			(\get_theme_mod('ae_splash_foreground', '') != '' ? 'background-image: url(\''.\wp_get_attachment_image_url(\get_theme_mod('ae_splash_foreground'), 'aether-splash-sd').'\');' : '').
			'min-height: 500px;'.
		'">';
		*/
		
	echo '</div>';
	echo '</section>';

	endif;
	
	if (!defined('AE_USE_CUSTOM_BOOKENDS')) {
		get_template_part( 'parts/content-start' );
		echo '<main class="'.ae_get_main_classes().'">';
	}
