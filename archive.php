<?php
/**
 * The template for displaying Archive pages.
 *
 * @package aether
 */

	ae_use_custom_content_bookends();
	get_header(); 

	if (have_posts()) {
			get_template_part( 'archive-resultlist' );
	}
	else {
			get_template_part( 'archive-noresults' );
	}

	get_footer(); 
