<?php
/**
 * aether functions and definitions
 *
 * @package aether
 */
 
namespace Aether;

//\show_admin_bar(false);

// Widgets
//require_once(get_template_directory().'/inc/widgets/widget-categories.php');
//require_once(get_template_directory().'/inc/widgets/widget-social.php');
//require_once(get_template_directory().'/inc/widgets/widget-recent-posts.php');

// Utilities
require_once(get_template_directory().'/includes/utilities.php');
require_once(get_template_directory().'/includes/sanitizers.php');
require_once(get_template_directory().'/includes/template-functions.php');
//require_once(get_template_directory().'/includes/async.php');
require_once(get_template_directory().'/inc/template-tags.php'); // Custom template tags for this theme.
//require_once(get_template_directory().'/inc/extras.php'); // Custom functions that act independently of the theme templates.

//require_once(get_template_directory().'/inc/jetpack.php'); // Load Jetpack compatibility file.
//require_once(get_template_directory().'/inc/metaboxes.php'); // Load custom metabox
//require_once(get_template_directory().'/inc/socialnav.php'); // Social Nav Menu

//require_once(get_template_directory().'/inc/customizer.php'); // Customizer additions.

// Classes
require_once(get_template_directory().'/classes/content-layout.php');
require_once(get_template_directory().'/classes/primary-nav-walker.php');
require_once(get_template_directory().'/classes/footer-nav-walker.php');

// Components
require_once(get_template_directory().'/classes/primary-header.php');
require_once(get_template_directory().'/classes/frontpage-splash.php');
require_once(get_template_directory().'/classes/kde-org-addons.php');
//require_once(get_template_directory().'/classes/footer-nav-walker.php');

	
	
	//if (\get_theme_mod('ae_hide_native_admin_bar', '1') == '1') {
//		\show_admin_bar(false);
	//}

	
	
/**
 * Register general support options
 * @link https://developer.wordpress.org/reference/functions/add_theme_support/
 */
function register_support()
{
	// Translation support
	load_theme_textdomain( 'aether', get_template_directory() . '/languages' );
	
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Allow wordpress to provide title tag content
	add_theme_support( 'title-tag' );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', [
		'video',
		'audio',
	]);

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', [
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
		'caption',
	]);
}

add_action( 'after_setup_theme', 'Aether\\register_support' );


/**
 * Registers navigation manu areas supported by the Aether theme.
 * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
 * 
 * Primary: The top-level navigation at the head of the site. Supports 3 levels
 * deep from the header, and further levels on the site map.
 * 
 * Footer: displayed at the bottom of every page. Supports 2 levels, and is NOT
 * further displayed in the site map.
 */
function register_navigation()
{
	register_nav_menus([
		'primary'  => esc_html__( 'Primary Menu', 'aether' ),
		'footer' => esc_html__( 'Footer Links', 'aether' )
	]);
	
	
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'aether' ),
		'id'            => 'ae-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
}

add_action( 'after_setup_theme', 'Aether\\register_navigation' );


/**
 * Registers media support
 * @link https://developer.wordpress.org/reference/functions/add_theme_support/
 * @link https://developer.wordpress.org/reference/functions/add_image_size/
 *
 * Primary: The top-level navigation at the head of the site. Supports 3 levels
 * deep from the header, and further levels on the site map.
 * 
 * Footer: displayed at the bottom of every page. Supports 2 levels, and is NOT
 * further displayed in the site map.
 */
function register_media_support()
{
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'aether-primary-menu', 640, 480, true );
	add_image_size( 'aether-primary-menu-hd', 1280, 940, true );

	/*
	add_image_size( 'aether-featured', 1170, 550, true );
	add_image_size( 'aether-slider', 1920, 550, true );
	add_image_size( 'aether-thumbnail', 330, 220, true );
	add_image_size( 'aether-medium', 640, 480, true );
	*/
}

add_action( 'after_setup_theme', 'Aether\\register_media_support' );


/**
 * Enqueue scripts and styles.
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_style/
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_script/
 */
function register_assets() 
{
	// Universal CSS documents
	wp_enqueue_style( '3p-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'ae-reset',     get_template_directory_uri() . '/css/reset.css' );
	wp_enqueue_style( 'ae-glyphs',    get_template_directory_uri() . '/css/glyph.css' );
	wp_enqueue_style( 'ae-social',    get_template_directory_uri() . '/css/social.css' );
	wp_enqueue_style( 'ae-header',    get_template_directory_uri() . '/css/header.css' );
	wp_enqueue_style( 'ae-main',      get_template_directory_uri() . '/css/main.css' );
	wp_enqueue_style( 'ae-footer',    get_template_directory_uri() . '/css/footer.css' );
	wp_enqueue_style( 'ae-slideshow', get_template_directory_uri() . '/css/slideshow.css' );
	wp_enqueue_style( 'ae-style',     get_stylesheet_uri() );

	// Conditional CSS
	//if( ( is_home() || is_front_page() ) && get_theme_mod('ae_featured_hide') == 1 ) {
	//	wp_enqueue_style( '3p-flexslider', get_template_directory_uri().'/css/flexslider.css' );
	//}

	// Universal scripts
	wp_enqueue_script('3p-modernizr',   get_template_directory_uri().'/js/modernizr.min.js', array('jquery') );
	wp_enqueue_script('3p-bootstrapjs', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery') );
	wp_enqueue_script('ae-dropdowns',   get_template_directory_uri().'/js/header-dropdowns.js', array('jquery') );
	wp_enqueue_script('ae-cookies',     get_template_directory_uri().'/js/cookie.js', array('jquery') );
	//wp_enqueue_script('ae-search',        get_template_directory_uri().'/js/header-search.min.js', array('jquery') );
	//wp_enqueue_script('ae-dropdowns',     get_template_directory_uri().'/js/header-dropdowns.min.js', array('jquery') );
	//wp_enqueue_script('ae-functions',     get_template_directory_uri().'/js/functions.min.js', array('jquery') );
	
	//if (is_customize_preview()) {
	//		wp_enqueue_script('ae-customizer', get_template_directory_uri().'/js/customizer.js', array('jquery') );
	//}
	
	
	// Optional Stuffs
	if (\get_theme_mod('ae_enable_splash', '1') == '1' || is_customize_preview()) {
		wp_enqueue_style('ae-splash', get_template_directory_uri().'/css/splash.css');
	}
	
	if (\get_theme_mod('ae_option_livesearch', '1') == '1' || is_customize_preview()) {
		wp_enqueue_script('ae-search', get_template_directory_uri().'/js/header-search.js', array('jquery') );
		wp_localize_script('ae-search', 'aetherJsConfig', [
			'some_string' => __( 'Some string to translate', 'plugin-domain' ),
			'a_value' => '10'
		]);
	}
	//wp_enqueue_script('ae-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20140222', true );

	// Add slider JS only if is front page ans slider is enabled
	if( ( is_home() || is_front_page() ) && get_theme_mod('ae_featured_hide') == 1 ) {
		wp_register_script( 'flexslider-js', get_template_directory_uri() . '/js/flexslider.min.js', array('jquery'), '20140222', true );
	}

	// Threaded comments
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'Aether\\register_assets' );


/**
 * Allow smoother editing in the live sire preview.
 */
function customize_previewer()
{
	wp_enqueue_script( 'ae_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), false, true );
}

add_action( 'customize_preview_init', 'Aether\\customize_previewer' );


/**
 * Register widgetized area and update sidebar with default widgets.
 *
 * TODO: Mess with the widgets.
 */
//function widgets_init() {
/*
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'aether' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_widget( 'ae_social_widget' );
	register_widget( 'ae_recent_posts' );
	register_widget( 'ae_categories' );
	*/
//}

//add_action( 'widgets_init', 'Aether\\widgets_init' );


/**
 * End of File
 */
