function SetCookie(c_name,value,expiredays)
{
	var exdate=new Date()
	exdate.setDate(exdate.getDate()+expiredays)
	document.cookie=c_name+ "=" +escape(value)+";path=/"+((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
}


(function( $ )
{
	$(document).scroll(function() { 
		if($(window).scrollTop() !== 0) {
			$("#cookieWarning").addClass('hidden');
		}
		else {
			$("#cookieWarning").removeClass('hidden');
		}
	});
	
	$(document).ready (function () 
	{
		if( document.cookie.indexOf("cookiebannerdismissed") ===-1 ){
				$("#cookieWarning").show();
				
				$("#removecookie").click(function () {
					SetCookie('cookiebannerdismissed','kde',365*10);
					$("#cookieWarning").remove();
				});
			} else
			{
				$("#cookieWarning").remove();
			}
			
	});
})( jQuery );
