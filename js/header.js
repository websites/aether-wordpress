(function ( $ ) {
	var date = new Date();
	var searchTimer;
	var lastKey;
	var currentSearchValue = '';
	var minLength = 2;
	var request;
	var timeMultiplierAfterPressingSpace = 1.5;
	var characterTimes = [1250, 1000, 800, 650, 500, 400, 350];
	var searchCache = {};
	var headerNavWidth = 0;
	
	if (typeof(Storage) !== "undefined" && typeof(localStorage.searchCache) !== "undefined") {
		searchCache = JSON.parse(localStorage.searchCache);
		cleanCache ();
	}
	
	$(window).resize(function() {
		recalculateHeader();
		adjustDropMenus();
	});
	
	function dismissSearch() {
		if ($('#header-search-input').is(':focus')) {
			$('body').focus();
		}
		$('#primary-header').removeClass('search-mode');
		$('#header-search-submit').prop('disabled', true);
		$('#header-search-input').val('');
		$('#header-search-form').removeClass();
		$('#header-search-results').removeClass().empty();
	}
	
	function recalculateHeader () {
		var oldState = $('#primary-header').hasClass('hamburger-mode');
		
		var headerInnerWidth  = $('#header-container').innerWidth();
		var headerLogoWidth   = $('#header-logo').outerWidth();
		var headerRightWidth  = $('#header-right-content').outerWidth();
		var headerSearchWidth = $('#header-search-mode-toggle').outerWidth();
		var totalWidth = headerNavWidth + headerLogoWidth + headerRightWidth + headerSearchWidth;
		
		// The magic number 110 is for spaces or pixel imperfections which may
		// cause errors when on the edge of going into burger mode.
		// In the future it should calculate based on the next smallest breakpoint.
		var newState = headerInnerWidth < totalWidth + 110;
		
		if (newState != oldState) {
			$('#header-nav').removeClass('active');
		}
		
		$('#primary-header').toggleClass('hamburger-mode', newState);
	}
	
	function cleanCache () {
		for (var i in searchCache) {
			if (searchCache[i]['time'] < date.getTime() - 1000 * 60 * 60 * 2)
				delete searchCache[i];
		}
		
		localStorage.searchCache = JSON.stringify(searchCache);
	}
	
	function adjustDropMenus () {
		var navRightEdge = $('#header-container').offset().left + $('#header-container').width();
		
		$('.menu-item-depth-0 > .dropdown-menu').each(function () {
			var myRightEdge = $(this).prev('.dropdown-toggle').offset().left + $(this).width();
			var offset = $(window).width() - myRightEdge - 15;
			
			$(this).css('margin-left', (offset > 0 ? 0 : offset) + 'px');
		});
	}
	
	function runSearch () {
		var cleanInput = $('#header-search-input').val().trim().toLowerCase();
		var length = cleanInput.length;
		
		if(request && request.readyState != 4){
				request.abort();
		}
		
		if (length < minLength) {
			if (length == 0) {
				$('#header-search-results').empty();
			}
			
			$('#header-search-form').removeClass();
			return;
		}
		
		$('#header-search-form').removeClass();
		$('#header-search-form').addClass('waiting');
		
		request = $.ajax({
			url: aetherJsConfig.url,
			data: {
				'action':'search',
				's' : cleanInput
			},
			success: function(data) {
				if (data['cache'] === true) {
					data['time'] = date.getTime();
					searchCache[cleanInput] = data;
					localStorage.searchCache = JSON.stringify(searchCache);
				}
				
				displayData(data);
			},
			error: function(errorThrown){
				$('#header-search-results').empty().removeClass();
				$('#header-search-form').removeClass();
			},
			complete: function () {
				currentSearchValue = $('#header-search-input').val();
			}
		});
	}
	
	function displayData (data) {
		$('#header-search-results').empty().removeClass().addClass('show-results');
		$('#header-search-form').removeClass();
		
		if (data['message']) {
			$('#header-search-results').append(
				$('<i>')
					.text(data['message'])
			);
		}
		
		for (var i in data['results']) {
			var result = data['results'][i];
			$('#header-search-results').append(
				$('<a>')
					.attr('href', result['url'])
					.text(result['title'])
			);
		}
	}
	
	$(document).ready(function() 
	{
		headerNavWidth = $('#header-nav').outerWidth();
		recalculateHeader();
		adjustDropMenus();
		
		$('#header-menu-button').click(function(){
			$('#header-nav').toggleClass('open');
		});
		
		$('#header-search-mode-toggle').click(function( event ) {
			$('#header-nav').removeClass('open');
			if ($('#primary-header').hasClass('search-mode')) { 
				dismissSearch();
			}
			else {
				$('#header-search-input').val('').focus();
				$('#header-search-form').addClass('just-started');
				$('#primary-header').addClass('search-mode');
			}
		});
		
		$('#header-search-submit').click(function( event ) {
			if ($('#header-search-input').val().trim().length < minLength) {
				$('#header-search-form').submit();
			}
		});
		
		$(document).click(function(event) { 
			if(!$(event.target).closest('#header-search-form').length && !$(event.target).closest('#header-search-mode-toggle').length) {
				if($('#primary-header').hasClass('search-mode')) {
					dismissSearch();
				}
			}
		});
		
		$('#header-search-input').on('keydown', function (event) {
			if (searchTimer) {
				clearTimeout(searchTimer);
			}
			lastKey = String.fromCharCode(event.which);
		});
		
		$('.dropdown-menu').on('click', function (event) {
			event.stopPropagation();
		});
		
		/*
		 * This function will put in a delay from the last keypress to the start
		 * of making a request. If the user has typed fewer characters it will
		 * allot more time to avoid extra requests wile the user is beginning their
		 * search. It will also allow extra time after non-alphanumeric characters
		 * have been typed.
		 */
		$('#header-search-input').on('keyup', function () {
			var length = $('#header-search-input').val().trim().length;
			var charCounter = length - minLength;
			var cleanInput = $('#header-search-input').val().trim().toLowerCase();
			
			$('#header-search-submit').prop('disabled', length < minLength);
			
			if (typeof searchCache[cleanInput] !== 'undefined') {
				displayData(searchCache[cleanInput]);
				return;
			}
			
			if (currentSearchValue.trim() == $('#header-search-input').val().trim()) {
				return;
			}
			
			if (charCounter >= characterTimes.length) {
				charCounter = characterTimes.length - 1;
			}
			
			if (typeof timer_gear !== "undefined") {
				clearTimeout(searchTimer);
			}
			
			var timeToWait = !lastKey.match(/[0-9a-zA-Z]/) ? characterTimes[charCounter] * timeMultiplierAfterPressingSpace : characterTimes[charCounter];
			searchTimer = setTimeout(runSearch, timeToWait);
		});
		
		
		$( ".persistent-panel-menu" ).on( "click", "a", function() {
			if ($(this).attr('href') && $(this).attr('target') == '_blank') {
				window.open($(this).attr('href'), '_blank');
			}
			else if ($(this).attr('href')) {
				window.location.href = $(this).attr('href');
			}
		});
	});
}( jQuery )); 
