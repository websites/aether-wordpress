(function ( $ ) {
	var m_date = new Date();
	var m_searchTimer;
	var m_lastKeyPressed;
	var m_minSearchLength = 2;
	var m_xhrRequest;
	var m_nonAlphaNumMultiplier = 1.5;
	var m_searchDelayCurve = [1250, 1000, 800, 650, 500, 400, 350];
	var m_cache = {};
	
	var $_header  = null;
	var $_submit  = null;
	var $_input   = null;
	var $_form    = null;
	var $_results = null;
	
	if (typeof(Storage) !== "undefined" && typeof(localStorage.searchCache) !== "undefined") {
		m_cache = JSON.parse(localStorage.searchCache);
		cleanCache ();
	}
	
	function dismissSearch() 
	{
		if ($_input.is(':focus')) {
			$('body').focus();
		}
		$_header.removeClass('search-mode');
		$_submit.prop('disabled', true);
		$_input.val('');
		$_form.removeClass();
		$_results.removeClass().empty();
	}
	
	function cleanCache () 
	{
		for (var i in m_cache) {
			if (m_cache[i]['time'] < m_date.getTime() - 1000 * 60 * 60 * 2)
				delete m_cache[i];
		}
		
		localStorage.searchCache = JSON.stringify(m_cache);
	}
	
	function cacheResult (key, data) 
	{
		if (!aetherJsConfig.localCache) {
			return;
		}
		
		data['time'] = m_date.getTime();
		m_cache[key] = data;
		localStorage.searchCache = JSON.stringify(m_cache);
	}
	
	function runSearch () 
	{
		var cleanInput = $_input.val().trim().toLowerCase();
		var length = cleanInput.length;
		
		if(m_xhrRequest && m_xhrRequest.readyState != 4){
				m_xhrRequest.abort();
		}
		
		if (length < m_minSearchLength) {
			if (length == 0) {
				$_results.empty();
			}
			
			$_form.removeClass();
			return;
		}
		
		$_form.removeClass();
		$_form.addClass('waiting');
		
		m_xhrRequest = $.ajax({
			url: aetherJsConfig.url,
			data: {
				'action':'search',
				's' : cleanInput
			},
			success: function(data) {
				cacheResult(cleanInput, data);
				displayData(data);
			},
			error: function(errorThrown){
				$_results.empty().removeClass();
				$_form.removeClass();
			}
		});
	}
	
	function displayData (data) 
	{
		$_results.empty().removeClass().addClass('show-results');
		$_form.removeClass();
		
		if (data['message']) {
			$_results.append(
				$('<i>')
					.text(data['message'])
			);
		}
		
		for (var i in data['results']) {
			var result = data['results'][i];
			$_results.append(
				$('<a>')
					.attr('href', result['url'])
					.text(result['title'])
			);
		}
	}
	
	$(document).ready(function() 
	{
		$_header  = $('#primary-header');
		$_submit  = $('#header-search-submit');
		$_input   = $('#header-search-input');
		$_form    = $('#header-search-form');
		$_results = $('#header-search-results');
		
		$('#header-search-mode-toggle').click(function( event ) 
		{
			$('#header-nav').removeClass('open');
			if ($_header.hasClass('search-mode')) { 
				dismissSearch();
			}
			else {
				$_input.val('').focus();
				$_form.addClass('just-started');
				$_header.addClass('search-mode');
			}
		});
		
		$_submit.click(function( event ) 
		{
			if ($_input.val().trim().length < m_minSearchLength) {
				$_form.submit();
			}
		});
		
		$(document).click(function(event) 
		{ 
			if(!$(event.target).closest('#header-search-form').length && !$(event.target).closest('#header-search-mode-toggle').length) {
				if($_header.hasClass('search-mode')) {
					dismissSearch();
				}
			}
		});
		
		$_input.on('keydown', function (event) 
		{
			if (m_searchTimer) {
				clearTimeout(m_searchTimer);
			}
			m_lastKeyPressed = String.fromCharCode(event.which);
		});
		
		/*
		 * This function will put in a delay from the last keypress to the start
		 * of making a request. If the user has typed fewer characters it will
		 * allot more time to avoid extra requests wile the user is beginning their
		 * search. It will also allow extra time after non-alphanumeric characters
		 * have been typed.
		 */
		$_input.on('keyup', function () 
		{
			var length = $_input.val().trim().length;
			var charCounter = length - m_minSearchLength;
			var cleanInput = $_input.val().trim().toLowerCase();
			
			$_submit.prop('disabled', length < m_minSearchLength);
			
			if (typeof m_cache[cleanInput] !== 'undefined' && aetherJsConfig.localCache) {
				displayData(m_cache[cleanInput]);
				return;
			}
			
			if (charCounter >= m_searchDelayCurve.length) {
				charCounter = m_searchDelayCurve.length - 1;
			}
			
			if (typeof timer_gear !== "undefined") {
				clearTimeout(m_searchTimer);
			}
			
			var timeToWait = !m_lastKeyPressed.match(/[0-9a-zA-Z]/) ? m_searchDelayCurve[charCounter] * m_nonAlphaNumMultiplier : m_searchDelayCurve[charCounter];
			m_searchTimer = setTimeout(runSearch, timeToWait);
		});
	});
}( jQuery )); 
