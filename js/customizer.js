(function ( $ ) {
/**
 * Async customisation utilities
 */

	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-name a' ).text( to );
		} );
	} );
	
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.tagline' ).text( to );
		} );
	} );
	

	/**
	 * Show / Hide the live search button
	 * @file /classes/primary-header.php
	 */
	wp.customize( 'ae_option_livesearch', function( transport ) {
		transport.bind( function( value ) {
			$('#header-search-mode-toggle').toggle(value);
		});
	});
	

	/**
	 * Show / Hide the user menu
	 * @file /classes/primary-header.php
	 */
	wp.customize( 'ae_option_usermenu', function( transport ) {
		transport.bind( function( value ) {
			$('#header-user-button').toggle(value);
		});
	});
	
}( jQuery )); 
