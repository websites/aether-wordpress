(function ( $ ) {
	var m_headerNavWidth = 0;
	
	function recalculateHeader () {
		var headerInnerWidth  = $('#header-container').innerWidth();
		var headerLogoWidth   = $('#header-logo').outerWidth();
		var headerRightWidth  = $('#header-right-content').outerWidth();
		var headerSearchWidth = $('#header-search-mode-toggle').outerWidth();
		var totalWidth = m_headerNavWidth + headerLogoWidth + headerRightWidth + headerSearchWidth;
		var newState   = headerInnerWidth < totalWidth + 110;
		// The magic number 110 is for spaces or pixel imperfections which may
		// cause errors when on the edge of going into burger mode.
		// In the future it should calculate based on the next smallest breakpoint.
		
		$('#primary-header').toggleClass('hamburger-mode', newState);
	}
	
	
	function adjustDropMenus () {
		var navRightEdge = $('#header-container').offset().left + $('#header-container').width();
		
		$('.menu-item-depth-0 > .dropdown-menu').each(function () {
			var myRightEdge = $(this).prev('.dropdown-toggle').offset().left + $(this).width();
			var offset = $(window).width() - myRightEdge - 15;
			
			$(this).css('margin-left', (offset > 0 ? 0 : offset) + 'px');
		});
	}
	
	
	$(window).resize(function() {
		recalculateHeader();
		adjustDropMenus();
	});
	
	
	$(document).ready(function() 
	{
		m_headerNavWidth = $('#header-nav').outerWidth();
		recalculateHeader();
		adjustDropMenus();
		
		$('#header-menu-button').click(function(){
			$('#header-nav').toggleClass('open');
		});
		
		$('.dropdown-menu').on('click', function (event) {
			event.stopPropagation();
		});
		
		$(".persistent-panel-menu").on( "click", "a", function() {
			if ($(this).attr('href') && $(this).attr('target') == '_blank') {
				window.open($(this).attr('href'), '_blank');
			}
			else if ($(this).attr('href')) {
				window.location.href = $(this).attr('href');
			}
		});
	});
}( jQuery )); 
