<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package aether
 */
 
	wp_enqueue_style( 'archive-resultlist', get_template_directory_uri() . '/css/archive-resultlist.css' );

	global $query_string;

?>
		<header class="archive-header">
			<h1 class="page-title">
				<?php
					if ( is_category() ) :
						single_cat_title();

					elseif ( is_tag() ) :
						single_tag_title();

					elseif ( is_author() ) :
						printf( esc_html__( 'Author: %s', 'aether' ), '<span class="vcard">' . get_the_author() . '</span>' );

					elseif ( is_day() ) :
						printf( esc_html__( 'Day: %s', 'aether' ), '<span>' . get_the_date() . '</span>' );

					elseif ( is_month() ) :
						printf( esc_html__( 'Month: %s', 'aether' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'aether' ) ) . '</span>' );

					elseif ( is_year() ) :
						printf( esc_html__( 'Year: %s', 'aether' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'aether' ) ) . '</span>' );

					elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
						esc_html_e( 'Asides', 'aether' );

					elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
						esc_html_e( 'Galleries', 'aether');

					elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
						esc_html_e( 'Images', 'aether');

					elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
						esc_html_e( 'Videos', 'aether' );

					elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
						esc_html_e( 'Quotes', 'aether' );

					elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
						esc_html_e( 'Links', 'aether' );

					elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
						esc_html_e( 'Statuses', 'aether' );

					elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
						esc_html_e( 'Audios', 'aether' );

					elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
						esc_html_e( 'Chats', 'aether' );

					else :
						esc_html_e( 'Archives', 'aether' );

					endif;
				?>
			</h1>
			<?php
				// Show an optional term description.
				$term_description = term_description();
				if ( ! empty( $term_description ) ) :
					printf( '<div class="taxonomy-description">%s</div>', $term_description );
				endif;
			?>
		</header><!-- .page-header -->
<?php
	get_template_part( 'parts/content-start' );
?>
<main class="<?php ae_main_classes(); ?>">
	<section class="archive-list">

		<?php /* Start the Loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<?php
				/* Include the Post-Format-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Format name) and that will be used instead.
					*/
				get_template_part( 'parts/list-excerpt', get_post_format() );
			?>

		<?php endwhile; ?>
	</section>


	<?php if ( $GLOBALS['wp_query']->max_num_pages > 1 ): ?>
		<nav class="pagination" role="navigation">
			<h1 class="screen-reader-text"><?php esc_html_e( 'Posts navigation', 'aether' ); ?></h1>
			<div class="nav-links generic-links">

				<?php if ( get_next_posts_link() ) : ?>
				<div class="prev"> <?php next_posts_link( __( 'Older posts', 'aether' ) ); ?></div>
				<?php endif; ?>

				<?php if ( get_previous_posts_link() ) : ?>
				<div class="next"><?php previous_posts_link( __( 'Newer posts', 'aether' ) ); ?> </div>
				<?php endif; ?>

			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
	<?php endif; ?>
</main>

<?php

	get_template_part( 'parts/content-end' );
