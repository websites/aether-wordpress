<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package aether
 */
 
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
 
 
	ae_use_custom_content_bookends();
	get_header();
	
	
	get_template_part( 'parts/content-start' );
	?><main class="<?php ae_main_classes(); ?>"><?php
	do_action('after_header');
	
	if (have_posts()) {
		echo '<section>';
		while (have_posts()) {
			the_post();
			get_template_part( 'content', '' );
		}
		
		echo '</section>';
	}
	else {
		get_template_part( 'content', 'none' );
	}
	

	 if ( $GLOBALS['wp_query']->max_num_pages > 1 ): ?>
		<nav class="pagination" role="navigation">
			<h1 class="screen-reader-text"><?php esc_html_e( 'Posts navigation', 'aether' ); ?></h1>
			<div class="nav-links generic-links">

				<?php if ( get_next_posts_link() ) : ?>
				<div class="prev"> <?php next_posts_link( __( 'Older posts', 'aether' ) ); ?></div>
				<?php endif; ?>

				<?php if ( get_previous_posts_link() ) : ?>
				<div class="next"><?php previous_posts_link( __( 'Newer posts', 'aether' ) ); ?> </div>
				<?php endif; ?>

			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
	<?php endif;
	
	
	echo '</main>';
	get_template_part( 'parts/content-end' );
	get_footer();

