<?php
/**
 * aether functions and definitions
 *
 * @package aether
 */
 
function ae_use_custom_content_bookends () {
    define('AE_USE_CUSTOM_BOOKENDS', true);
}
 
function ae_get_main_classes ($customClasses = null) {
    $standardClasses = ['container', 'primary-main'];
    
    if (is_array($customClasses)) {
        return implode(' ', array_merge($customClasses, $standardClasses));
    }
    else if (is_string($customClasses)) {
        return $customClasses.' '.implode(' ', $standardClasses);
    }
    else {
        return implode(' ', $standardClasses);
    }
}

function ae_main_classes ($customClasses = null) {
    echo ae_get_main_classes($customClasses);
}

/**
 * End of File
 */
