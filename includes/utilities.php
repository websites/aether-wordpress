<?php
/**
 * aether functions and definitions
 *
 * @package aether
 */
 
namespace Aether;

/**
 * Accepts an array of key->value pairs and compiles them into HTML formatted
 * tag attributes. E.g. [id=>'important', 'width'=>'5', 'text'=>'"'] will output
 * id="important" width="5" text="\""
 *
 * If a value for an attrbute is NULL, it will omit that attribute. It will 
 * convert boolean values to 0 and 1.
 * 
 * This function will not attempt to sanitize attribute names.
 */
function assembleAttributes ($attributes) {
	if (!is_array($attributes) || count($attributes) == 0) {
		return '';
	}
	
	$output = [];
	
	foreach ($attributes as $name => $value) {
		if ($value === null) {
			continue;
		}
		else if ($value === true) {
			$value = '1';
		}
		else if ($value === false) {
			$value = '0';
		}
		
		$output[] = $name.'="'.($name == 'href' ? esc_url($value) : esc_attr($value)).'"';
	}
	
	return implode(' ', $output);
}


function isExternalLink($url) {
	$link = parse_url($url);
	$host = parse_url(get_site_url());
	
	if (empty($link['host']) || $link['host'] == $host['host']) {
		return false;
	}

	return strrpos(strtolower($link['host']), $host['host']) !== strlen($link['host']) - strlen($host['host']); 
}


function getMenuStructure($slug) {
	$locations = get_nav_menu_locations();
	
	if (empty($locations) || !isset($locations[$slug])) {
		return false;
	}
	
	
	
	$menu = wp_get_nav_menu_object($locations[ $slug ]);
	return wp_get_nav_menu_items($menu->term_id);
}


/**
 * Takes a set of arguments (usually from something like $_GET) and return an
 * object conforming to the defaults provided, including type conversion.
 */
function struct ($args, $defaults = [])
{
	if (empty($args)) {
		settype($defaults, 'object');
		return $defaults;
	}

	// Convert the types if requested
	foreach ($defaults as $k => $v) {
		if (isset($args[$k])) {
			$defaults[$k] = convert($args[$k], $defaults[$k]);
		}
	}

	settype($defaults, 'object');
	return $defaults;
}

function convert ($var, $target)
{
	$to = gettype($target);

	if (in_array($to, ['integer','double','string'])) {
		settype($var, $to);
		return $var;
	}

	$from = gettype($var);

	// Do nothing if there is no conversion needed
	if ($from == $to && ($to != 'object')) {
		return $var;
	}

	if ($from == 'string') {
		switch ($to) {
			case 'boolean':
				return stripos('false', $var) !== false ? false : (bool)$var;

			case 'array':
				return explode(',', $var);

			case 'object':
				$class = get_class($target);
				return (object)parse_str($var);
		}
	}

	settype($var, $to);
	return $var;
}

/**
 * End of File
 */
