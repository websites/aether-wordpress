<?php
/**
 * aether functions and definitions
 *
 * @package aether
 */
 
namespace Aether;

/**
 * Accepts an array of key->value pairs and compiles them into HTML formatted
 * tag attributes. E.g. [id=>'important', 'width'=>'5', 'text'=>'"'] will output
 * id="important" width="5" text="\""
 *
 * If a value for an attrbute is NULL, it will omit that attribute. It will 
 * convert boolean values to 0 and 1.
 * 
 * This function will not attempt to sanitize attribute names.
 */
function sanitizeCheckbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}


/**
 * End of File
 */
