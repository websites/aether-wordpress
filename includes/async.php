<?php
/**
 * aether functions and definitions
 *
 * @package aether
 */
 
namespace Aether;

/**
 * Accepts an array of key->value pairs and compiles them into HTML formatted
 * tag attributes. E.g. [id=>'important', 'width'=>'5', 'text'=>'"'] will output
 * id="important" width="5" text="\""
 *
 * If a value for an attrbute is NULL, it will omit that attribute. It will 
 * convert boolean values to 0 and 1.
 * 
 * This function will not attempt to sanitize attribute names.
 */
function headerSearch () {
	global $wpdb;
	global $post;
	
	$params = struct($_GET, [
		's' => '',
		'page' => 1
	]);

	$results = [];
	
	$query = new \WP_Query([
		's' => $params->s,
		'posts_per_page' => 10,
		'orderby' => [
				'IF(wp_posts.post_type = \'app\', 0, 1)' => 'DESC',
				'IF(wp_posts.post_type = \'post\', 1, 0)' => 'DESC',
				'relevance' => 'DESC',
				'post_modified' => 'DESC',
				'post_title' => 'ASC',
		],
	]);
	
	while ($query->have_posts()) {
		if ( $post->post_type != 'post' ) {
			continue;
		}
			
		$query->the_post();
		
		$results[] = [
			'title' => get_the_title(),
			'author' => get_the_author(),
			'excerpt' => get_the_excerpt(),
			'url' => get_permalink(),
		];
	}
	
	rewind_posts();
	
	while ($query->have_posts()) {
		if ( $post->post_type == 'post' ) {
			continue;
		}
		
		$query->the_post();
		
		$results[] = [
			'title' => get_the_title(),
			'author' => get_the_author(),
			'excerpt' => get_the_excerpt(),
			'url' => get_permalink(),
		];
	}
	
	\wp_send_json([
		'message' => count($results) == 0 ? __('No results found.', 'aether') : '',
		'totalResults' => count($results),
		'results' => $results
	]);
}

add_action("wp_ajax_search", "Aether\\headerSearch");
add_action("wp_ajax_nopriv_search", "Aether\\headerSearch");

/**
 * End of File
 */
